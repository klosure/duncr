﻿
![](logo.png)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

> short for 'dungeon creator'

Generates random ASCII RPG dungeons.


## Motivation
I wanted a fun C project to work on, and something that I could potentially use now and expand later. Eventually I would like to include this in an ASCII roguelike game of my own creation. Make some super sweet maps! 


## Project Goals
1. Clean, safe, and portable C style
2. Commandline interface
3. Include portals, enemies, gold, items, etc.
4. Customization options

## Example Dungeon

```
( 31x31 grid)
-- Level 1: ----------------------------------------------------------
##############################
#     ###  ###################
##    # g  ###################
##        x###################
## #      ####################
##    ### ####################
####      ####################
#### #      ##################
####    ##   ^################
#       #    p################
#x        ##  ################
#  #   x  ####################
# v   ### ####################
#   ##### ####################
## ######   ##################
#########   ##################
##########   #################
#########    #################
#########    #################
########## x   ###############
##########   m   #    ########
########         x ## ########
######## # p       ## ########
######## #   h         #######
#######  #        ##   #######
#######      ## #   ##########
#######  #####      ##########
####### ####    x   ##########
#######               ########
##########           #########
##############################
```


## Prerequisites
+ [C99](https://clc-wiki.net/wiki/C_resources:Compilers) compliant compiler
+ POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)


## Build
```bash
git clone https://gitlab.com/klosure/duncr.git
cd duncr
make
make install # as root

# to make a debug version
make debug

# to make an optimized version
make opt
```


## Usage
```bash
# note: the order of arguments does not matter
duncr                     # use defaults (one 80x80 level)
duncr -g 100              # generate one 100x100 level
duncr -g 73 -n 12         # generate twelve 73x73 levels
duncr -c 5                # generate a level with maximum handiCap
# handicap means more gold and items and less baddies..
duncr > my_dungeon.txt    # redirect the output to save the level

# Legend: 
# #: walls
#  : floor (empty space)
# h: hero
# x: enemy
# ^: stairs up
# v: stairs down
# t: trap (gas, spikes, rope, fire)
# p: portal (transport to another portal on level)
# g: gold
# m: merchant (buy items)

```

</br>


## TODO
Improve tile placement probability code.


## License
[BSD 3-clause](LICENSE.md)
