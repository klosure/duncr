
/*  duncr - header file
 *  author: klosure
 *  license: BSD 3-Clause
 */

#ifndef DUNCR
#define DUNCR

#define _POSIX_C_SOURCE 2
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <stdbool.h> // C99 required

typedef unsigned short int uint16_t;

// application constants
const char* VERSION = "0.2";
const uint16_t INVERSE_MULT = 999;
const uint16_t ITEM_PROB = 1;           // one percent
const uint16_t GOLD_PROB = 1;           // one percent
const uint16_t TRAP_PROB = 1;           // one percent
const uint16_t MERC_PROB = 1;           // one percent
const uint16_t PORTAL_PROB = 3;         // three percent
const uint16_t STAIRS_PROB = 4;         // four percent
const uint16_t ENEMY_PROB = 15;         // fifteen percent
const uint16_t DEFAULT_HANDICAP = 0;    // make level easier (max 5)
const uint16_t DEFAULT_GRID_SIZE = 60;  // 60x60 grid
const uint16_t DEFAULT_NUM_LEVELS = 1;  // how many levels to generate at once

// these tiles have the highest chance of being placed
struct common_tiles {
  char enemy;  // baddie
  char floor;  // blank == ' ' == floor
  char portal; // (one per dungeon walker)
};

// these tiles occur less often
struct rare_tiles {
  char gold; // $$$
  char item; // weapon, potion, artifact, etc
  char trap; // gas, spike, rope
  char merc; // merchant to buy items from
};

// these tiles are only used once per level
struct single_use_tiles {
  char su;    // stairs up
  char sd;    // stairs down
  char hero;  // YOU
};

// cartesian coordinates
struct coord {int x; int y;};

// hold the info on what tiles have been placed on the map
struct placement {bool hero; bool portal; bool stair_down; bool stair_up; bool merc_placed;};

// init tiles
const struct common_tiles S_CT = {'x',' ','p'};
const struct rare_tiles S_RT = {'g','i','t','m'};
const struct single_use_tiles S_SUT = {'^','v','h'};

/*
 * Fills in the built level (with walls exclusively)
 */
void fill_level(uint16_t max, char **level, uint16_t row, uint16_t col);

/*  Check if we are going to overwrite
 *  one of our specially placed chars.
 *  e.g v, ^, or p
*/
bool overwrite_check(char c);

// Place our items based on their probabilities
char try_roll_item(struct placement *placed, uint16_t handicap);

/*
 *  Our level is currently full of walls.
 *  Blaze a trail for us on the level (breaking down walls).
 *  On each function call we pick a cardinal direction randomly,
 *  move there, and chose a tile to place.
 *  North = 1, East = 2, South = 3, West = 4
 */
void walk(uint16_t size, char **level, struct coord pos, uint16_t moves_remaining,
	  struct placement *placed, uint16_t hc);

// run the program (called by main)
bool run(uint16_t gs, uint16_t nl, uint16_t hc);

// Gets a random number from 1-n
uint16_t roll(uint16_t n);

// Print each completed level (nl = number of level)
bool print_level(uint16_t size, char **level, uint16_t nl);

// Prints out the version of duncr
void print_version();

// Print the help info
void print_help();

// Check user input grid size: Where 30<gs<4096
bool grid_size_ok(uint16_t gs);

// Check user input for handicap range: Where 0<hc<6
bool handicap_range_ok(uint16_t hc);

// Check user input for number of levels: Where 0<nl<65000
bool num_levels_ok(uint16_t nl);

// Print an error message
int print_intput_error();

#endif // DUNCR
