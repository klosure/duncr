
/*   duncr - implementation file
 *   @author: klosure
 *   license: BSD 3-Clause
 */

#include "duncr.h"

static inline uint16_t make_even(uint16_t n) {
  return n - (n % 2);
}

bool grid_size_ok(uint16_t gs) {
  if(30 < gs && gs < 4096) {
    return true;
  }
  return false;
}


bool handicap_range_ok(uint16_t hc) {
  if(0 < hc && hc < 6) {
    return true;
  }
  return true;
}


bool num_levels_ok(uint16_t nl) {
  if(0 < nl && nl < USHRT_MAX) {
    return true;
  }
  return false;
}


void fill_level(uint16_t size, char **level, uint16_t row, uint16_t col) {
  if((row == size-1) && (col == size-1)) {
    // fill in last character then we are done
    level[row][col] = '\n';
    return;
  }
  if(col == size-1) {
    level[row][col] = '\n';
    ++row;
    col = 0;
  }
  else {
    level[row][col] = '#';
    ++col;
  }
  fill_level(size,level,row,col);
}


bool overwrite_check(char c) {
  if(c != 'p' && c != 'v' && c != '^'
     && c != 'm' && c != 'h' && c != 'i'
     && c != 'g') {
    return true;
  }
  return false;
}


char try_roll_item(struct placement *placed, uint16_t handicap) {
  char ret_char = ' ';
  uint16_t rolled = roll(INVERSE_MULT);
  uint16_t inv = INVERSE_MULT - rolled;
  if(inv <= ITEM_PROB+handicap) {
    // pick random one since they are same prob 
    switch(roll(4)) {
    case 1:
      ret_char = S_RT.item;
      break;
    case 2:
      ret_char = S_RT.gold;
      break;
    case 3:
      ret_char = S_RT.trap;
      break;
    case 4:
      if(!placed->merc_placed) {
        ret_char = S_RT.merc;
        placed->merc_placed = true;
      }
      break;
    }
  }
  else if(inv <= PORTAL_PROB && !placed->portal) {
    // portal has not been placed, and we rolled high enough
    ret_char = S_CT.portal;
    placed->portal = true;
  }
  else if(inv <= STAIRS_PROB && (!placed->stair_down || !placed->stair_up)) {
    // rolled high enough to place a stair tile
    if(!placed->stair_down) {
      // place stair down
      ret_char = S_SUT.sd;
      placed->stair_down = true;
    }
    else if(!placed->stair_up) {
      // place stair up
      ret_char = S_SUT.su;
      placed->stair_up = true;
    }
  }
  else if(inv <= ENEMY_PROB-handicap) {
    // rolled high enough to place enemy tile
    ret_char = S_CT.enemy;
  }
  
  return ret_char;
}


void walk(uint16_t size, char **level, struct coord pos , uint16_t moves_remaining,
	  struct placement *placed, uint16_t hc) {
  
  if(!moves_remaining) {
    // we have exhausted our moves
    return;
  }
  if(!placed->hero) {
    // first tile placement
    level[pos.x][pos.y] = S_SUT.hero;
    placed->hero = true;
  }
  
  // here we choose our cardinal direction
  uint16_t direction = roll(4);
  
  switch(direction) {
  case 1: // North
    if(pos.x-1 == 0) break;
    if(overwrite_check(level[pos.x-1][pos.y])) {
      level[--pos.x][pos.y] = try_roll_item(placed, hc);
    }
    break;
  case 2: // East
    if(pos.y+1 == size-2) break;
    if(overwrite_check(level[pos.x][pos.y+1])) {
      level[pos.x][++pos.y] = try_roll_item(placed, hc);
    }
    break;
  case 3: // South
    if(pos.x+1 == size-1) break;
    if(overwrite_check(level[pos.x+1][pos.y])) {
      level[++pos.x][pos.y] = try_roll_item(placed, hc);
    }
    break;
  case 4: // West
    if(pos.y-1 == 0) break;
    if(overwrite_check(level[pos.x][pos.y-1])) {
      level[pos.x][--pos.y] = try_roll_item(placed, hc);
    }
    break;
  }
  /* if we haven't set portal or stairs yet, do it now!
   * we dont want to run out of moves before we have placed these
   */
  if(moves_remaining < 4 && (!placed->portal || !placed->stair_down || !placed->stair_up)) {
    if(!placed->portal) {
      level[pos.x][pos.y] = S_CT.portal;
      placed->portal = true;
    }
    else if(!placed->stair_up) {
      level[pos.x][pos.y] = S_SUT.su;
      placed->stair_up = true;
    }
    else if(!placed->stair_down) {
      level[pos.x][pos.y] = S_SUT.sd;
      placed->stair_down = true;
    }
  }
  --moves_remaining;
  walk(size, level, pos, moves_remaining, placed, hc);
}


uint16_t roll(uint16_t n) {
  // return a random number between 1-n
  return (uint16_t)(rand() % n + 1);
}


bool print_level(uint16_t size, char **level, uint16_t nl) {
  printf("-- Level %d: ----------------------------------------------------------\n", nl);
  for(uint16_t r=0; r < size; ++r) {
    for(uint16_t c=0; c < size; ++c) {
      printf("%c",level[r][c]);
    }
  }
  printf("\n");
  return true;
}


void print_version() {
  printf("version: %s\n", VERSION);
}


int print_input_error(const char *message) {
  printf("There was an illegal value input:\n");
  printf("%s\n",message);
  print_help();
  return 1;
}


void print_help() {
  printf("duncr - the random dungeon creator\n");
  print_version();
  printf("author: klosure\n");
  printf("site: https://gitlab.com/klosure/duncr\n");
  printf("usage: -------------------------------\n");
  printf("$ duncr         # create a map of default size\n");
  printf("$ duncr -n 9    # create 9 maps of default size\n");
  printf("$ duncr -g 45   # create a 45x45 map\n");
  printf("$ duncr -c 5    # create a map with max handiCap of +5 (0 default)\n");
  printf("$ duncr -h      # see this help message\n");
  printf("$ duncr -v      # show the version info\n");
  printf("\n");
  printf("legend: -------------------------------\n");
  printf("#: walls \n");
  printf(" : floor (empty space) \n");
  printf("h: hero \n");
  printf("x: enemy \n");
  printf("^: stairs up \n");
  printf("v: stairs down \n");
  printf("t: trap (gas, spikes, rope, fire) \n");
  printf("p: portal (transport to another portal on level) \n");
  printf("g: gold \n");
  printf("m: merchant (buy items) \n");
}


bool run(uint16_t gs, uint16_t nl, uint16_t hc) {
  bool ran_ok = false;
  // while there are levels to print; print em!
  while(nl--) {
    struct coord pos = {0,0};
    struct placement placed = {false,false,false,false,false};
    char **level;

    // allocate
    level = malloc(gs * sizeof(*level));
    for(int k=0; k < gs; ++k) {
      level[k] = malloc(gs * sizeof( *(level[k])) );
    }
  
    fill_level(gs, level, 0, 0);
  
    // random starting row and col
    pos.x = roll(gs-2);
    pos.y = roll(gs-3);
    uint16_t num_walkers = make_even(gs / 10);
    // moves per walker 
    uint16_t mpw = gs*10;

    walk(gs, level, pos, mpw, &placed, hc);
    placed.hero = placed.stair_down = placed.stair_up = true;
    // hero and stairs were placed the first time
    while(num_walkers-1) {
      placed.portal = false;
      walk(gs, level, pos, mpw, &placed, hc);
      pos.x = roll(gs-2);
      pos.y = roll(gs-3);
      --num_walkers;
    }

    // print out our newly created level
    ran_ok = print_level(gs,level,nl+1);
 
    // deallocate
    for(int r=0; r < gs; ++r) {
      free(level[r]);
    }
    free(level);
    
    if((hc != 0) && (nl % 2 == 0)) --hc; // reduce handicap for every other level
    if(!ran_ok) {
      print_input_error("general error");
      return false;
    }
  } // end while loop
  return true;
}


int main(int argc, char** argv){
  // init rand() seed
  srand(time(NULL));
  bool ran_ok = false;
  uint16_t hc = DEFAULT_HANDICAP;
  uint16_t gs = DEFAULT_GRID_SIZE;
  uint16_t nl = DEFAULT_NUM_LEVELS;

  // grab any arguments passed in to us
  int opt;
  while ( (opt = getopt(argc, argv, "hvc:n:g:")) != -1) {
    switch (opt) {
      case 'h':
	print_help();
	return 0;
      case 'v':
	print_version();
	return 0;
      case 'g':
	if(grid_size_ok((uint16_t)atoi(optarg))) {
	  gs = (uint16_t)atoi(optarg);
	}
	else {
	  return print_input_error("Bad grid size\n");
	}
        break;
      case 'n':
	if(num_levels_ok((uint16_t)atoi(optarg))) {
	  nl = (uint16_t)atoi(optarg);
	}
	else {
	  return print_input_error("Too many/little levels requested (range 30 to ~65k\n)");
	}
        break;
      case 'c':
	if(handicap_range_ok((uint16_t)atoi(optarg))) {
	  hc = (uint16_t)atoi(optarg);
	}
	else {
	  return print_input_error("Handicap range needed from 1-5\n");
	}
	break;
    }
  }

  ran_ok = run(gs, nl, hc);

  if(!ran_ok) return 1;
  return 0;

}
