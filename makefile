CC=cc
RM=rm -f
PREFIX=/usr/local
CFLAGS=--std=c99 -pedantic -Wall -Wextra
LDFLAGS = -I include
LDFLAGS += -I tests
LDFLAGS += -I $(PREFIX)/include
LDLIBS=-L$(PREFIX)/lib

BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man/man1

.POSIX: all debug opt

all: duncr

debug: CFLAGS += -O0 -ggdb -save-temps -fno-omit-frame-pointer -fno-sanitize-recover
debug: clean duncr

opt: CFLAGS += --pipe -O2 -fpie -march=native -fstack-protector-strong
opt: LDFLAGS += -pie -Wl,-z,relro -Wl,-z,now
opt: clean duncr

duncr: duncr.o
	$(CC) $(LDFLAGS) -o duncr duncr.o $(LDLIBS)

duncr.o: duncr.c
	$(CC) $(LDFLAGS) $(CFLAGS) -c duncr.c

clean:
	$(RM) *.o *.s *.i

distclean: clean
	$(RM) *.tar.gz *~ duncr test_duncr

#test: test.c
#> run tests

dist: distclean
	tar -czvf ../duncr-source.tar.gz ./
	mv ../duncr-source.tar.gz ./

install:
	cp ./duncr $(BINDIR)
	cp ./duncr.1 $(MANDIR)

uninstall:
	rm $(BINDIR)/duncr
	rm $(MANDIR)/duncr.1
